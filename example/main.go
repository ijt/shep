package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

var sleep = flag.Duration("sleep", time.Second, "how long to sleep before opening lots of files")
var port = flag.Int("port", 8080, "port to listen on for HTTP requests")

func main() {
	flag.Parse()
	addr := fmt.Sprintf(":%d", *port)
	log.Printf("listening on http://localhost%s", addr)
	log.Fatal(http.ListenAndServe(addr, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s", r.Method, r.URL)
		switch strings.TrimSuffix(r.URL.Path, "/") {
		case "/leak":
			w.Write([]byte("leaking..."))
			go leak()
		case "/healthz":
			w.Write([]byte("ok"))
		case "/hang":
			w.Write([]byte("hanging..."))
			c := make(chan struct{})
			<-c
		}
	})))
}

// leak opens files until we run out of descriptors.
func leak() {
	var files []*os.File
	for {
		f, err := os.Open("/dev/null")
		if err != nil {
			log.Printf("failed to open /dev/null: %v", err)
			continue
		}
		files = append(files, f)
	}
}
