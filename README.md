Shep
====
Process shepherd, keeps your webserver alive.

Prerequisites
-------------
Install Rust to build the shep program, and Go to build the example web server to test it.

Video Demo
----------
https://youtu.be/idsPUkYeXq4

Example 
-------
In this directory run
```bash
cargo build
```

You can run an example web server under shep in the following way:
```bash
cd example
go build
cd ..
target/debug/shep run example/example --health-check-url http://localhost:8080/healthz/
```

Check that the leak server is up and running:
```bash
curl http://localhost:8080/
```

Have a look in the `shep-child-log-<number>` file for the output of the webserver.

This example involves a web server that normally returns "ok" from `/healthz/`, but if you hit `/leak/` it opens up more and more files in the background, and keeps going even after it runs out. Once that happens, the health check will fail, and Shep will restart the web server program. Let's try it:

```bash
curl http://localhost:8080/leak/
```

Look in `shep-log` and it will eventually say that Shep has restarted the web server. Try hitting the `/healthz/` endpoint again to check that it is fine:

```bash
curl http://localhost:8080/healthz/
```

You can also try this to get a better picture of what is going on:
1. In one terminal, run 
```bash
while true; do curl http://localhost:8080/healthz/; done
```
2. In another terminal, run
```bash
curl http://localhost:8080/leak/
```
3. Watch how the stream of "ok"s turns to error messages for a while when the process hits the file descriptor limit, and then goes back to "ok"s again after Shep restarts the web server.
